INF3180-TP1-oracle-sql-queries
==============================

Summary
---------

* SQL queries, some examples


Technologies used:
------------------

* Oracle-ex-11.2.0
* SQL


Database
---------

Script to create the database [script_bd.sql](script_bd.sql)


Reference
----------

INF3180 | 30 | Automme 2016 | Professeur: Fatiha Sadat


License
--------

This project is licensed under the Apache License - see [here](https://www.gnu.org/licenses/gpl-3.0.en.html) for details
