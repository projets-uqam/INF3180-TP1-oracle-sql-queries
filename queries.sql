
/***************
 * EXERCICE 1. *
 * REQ # 1.    *
 **************/

-- Les sigles et les titres des cours pris par l’étudiant Tremblay Jean
SELECT  C.SIGLE, C.TITRE
FROM    ETUDIANT E JOIN INSCRIPTION I ON E.CODEPERMANENT = I.CODEPERMANENT
        JOIN COURS C ON C.SIGLE = I.SIGLE
WHERE   E.NOM = 'Tremblay' and E.PRENOM = 'Jean';

/***************
 * EXERCICE 1. *
 * REQ # 2.    *
 **************/

-- Les codes permanents, les prénoms et les noms des étudiants qui sont inscrits aux cours
-- qui ont lieu «février 2004»
SELECT  DISTINCT(E.CODEPERMANENT), PRENOM, NOM
FROM    ETUDIANT E, INSCRIPTION I
WHERE   E.CODEPERMANENT = I.CODEPERMANENT AND I.CODESESSION = 12004;

/***************
 * EXERCICE 1. *
 * REQ # 3.    *
 **************/

-- La liste des sigles et numéro de groupe des cours suivis par Lucie Tremblay
-- à la session dont le codeSession est 32003
SELECT   I.SIGLE, I.NOGROUPE
FROM     ETUDIANT E, INSCRIPTION I
WHERE    E.CODEPERMANENT = I.CODEPERMANENT AND I.CODESESSION = 32003 AND
         E.PRENOM = 'Lucie' AND E.NOM = 'Tremblay';

/***************
 * EXERCICE 1. *
 * REQ # 4.    *
 **************/

-- Le code permanent et le nom des étudiants inscrits au cours INF3180 de la session 32003
-- et qui n’ont pas abandonné le cours durant cette session
SELECT   E.CODEPERMANENT, E.NOM
FROM     ETUDIANT E, INSCRIPTION I
WHERE    I.CODEPERMANENT = E.CODEPERMANENT AND I.SIGLE = 'INF3180' AND
         I.CODESESSION = 32003 AND I.DATEABANDON IS NULL;

/***************
 * EXERCICE 1. *
 * REQ # 5.    *
 **************/

-- Les matricules (code) et les noms des professeurs, qui ont enseigné au plus un
-- cours offert à la session 32003
SELECT DISTINCT(P.CODEPROFESSEUR), P.NOM
FROM   PROFESSEUR P, GROUPECOURS GC
WHERE  P.CODEPROFESSEUR = GC.CODEPROFESSEUR AND GC.CODESESSION = '32003';

/***************
 * EXERCICE 2. *
 * REQ # 6.    *
 **************/

-- Pour chacun des étudiants inscrits au programme à code 7416, donnez son prénom, son nom ainsi
-- que le nombre de crédits qu’il a accumulés.
-- N.B. Pour que le cours soit comptabilisé, il faut que la note obtenue soit d’au moins 70
SELECT   E.PRENOM, E.NOM, COUNT(E.CODEPERMANENT) * 3 AS CREDITS_ACCUMULES
FROM     ETUDIANT E, INSCRIPTION I
WHERE    E.CODEPERMANENT = I.CODEPERMANENT and I.NOTE >= 70 AND E.CODEPROGRAMME= 7416
GROUP BY E.PRENOM, E.NOM;

/***************
 * EXERCICE 2. *
 * REQ # 7.    *
 **************/

/*
Nous nous intéressons aux groupes cours de la session 32003 et ciblons ceux dont le nombre
d’étudiants ayant eu une note est supérieur à 0 . Uniquement pour ces groupes, donnez le sigle du
cours, le numéro du groupe, le nombre des inscrits, le nombre des étudiants ayant obtenu une
note et la note moyenne du groupe.
*/
SELECT   SIGLE, NOGROUPE, COUNT(DISTINCT CODEPERMANENT) AS ETUDIANTS_INSCRITS,
         COUNT(CASE WHEN NOTE IS NOT NULL THEN 1 ELSE NULL END)
		 AS ETUDIANTS_AVEC_UNE_NOTE,
         AVG (NOTE) AS MOYENNE_NOTES
FROM     INSCRIPTION
WHERE    CODESESSION = 32003
GROUP BY SIGLE, NOGROUPE;


/***************
 * EXERCICE 2. *
 * REQ # 8.    *
 **************/

-- Créer une vue MoyenneParGroupe qui affiche le sigle de cours, le numéro de groupe, le code de
-- la session et la moyenne des notes du groupe correspondant
CREATE VIEW MoyenneParGroupe AS
SELECT      SIGLE, NOGROUPE, CODESESSION, AVG (NOTE) AS MOYENNE_NOTES
FROM        INSCRIPTION
GROUP BY    SIGLE, NOGROUPE, CODESESSION;

SELECT * FROM MoyenneParGroupe;

/***************
 * EXERCICE 2. *
 * REQ # 9.    *
 **************/

-- Le code permanent et le nom des étudiants qui ont réussi les cours ou ils étaient inscrits
-- N.B. Pour réussir un cours, une note d’au moins 65/100 est nécessaire
SELECT   DISTINCT (E.CODEPERMANENT), E.NOM
FROM     ETUDIANT E, INSCRIPTION I
WHERE    E.CODEPERMANENT = I.CODEPERMANENT AND I.DATEINSCRIPTION IS NOT NULL
         AND I.NOTE >= 65;

/***************
 * EXERCICE 2. *
 * REQ # 10.   *
 **************/

-- Les sigles et titres des cours dont le titre contient le mot « programmation » mais pas le mot «objet»
SELECT  SIGLE, TITRE
FROM    COURS
WHERE   UPPER (TITRE) LIKE '%PROGRAMMATION%' AND
        UPPER (TITRE) NOT LIKE '%OBJET%';

/***************
 * EXERCICE 2. *
 * REQ # 11.   *
 **************/

-- Pour chaque étudiant dont la moyenne est supérieure à 80 pour les cours suivis à la session
-- 32003, afficher le code permanent avec la moyenne
SELECT CODEPERMANENT, AVG (NOTE) AS MOYENNE_NOTES
FROM  INSCRIPTION
WHERE CODESESSION = 32003
GROUP BY CODEPERMANENT
HAVING AVG (NOTE) > 80;

/***************
 * EXERCICE 2. *
 * REQ # 12.   *
 **************/

/*
Pour chacun des professeurs, afficher son nom, prénom et nombre de crédits de cours donnés (il
faut additionner les crédits de cours de chacun des groupes auquel le professeur est assigné). Si le
professeur n’a pas donné de cours, il faut l’afficher avec un nombre de crédits égal à 0. Le
résultat sera trié en ordre décroissant du nombre de crédits et pour un nombre de crédits
particulier, en ordre croissant du nom et prénom
*/
SELECT    P.NOM, P.PRENOM, COUNT(CASE WHEN P.CODEPROFESSEUR IN(SELECT GC.CODEPROFESSEUR
          FROM GROUPECOURS GC ) THEN 1 ELSE NULL END) * 3 AS CREDITS_COURS_DONNES
FROM      PROFESSEUR P
LEFT JOIN GROUPECOURS GC
ON        P.CODEPROFESSEUR = GC.CODEPROFESSEUR
GROUP BY  P.NOM, P.PRENOM
ORDER BY  CREDITS_COURS_DONNES DESC, P.NOM ASC, P.PRENOM ASC;

/***************
 * EXERCICE 2. *
 * REQ # 13.   *
 **************/

/*
Afficher les détails des abandons qui ont eu lieu plus de deux semaines (15 jours et plus) après le
début de la session. Afficher le codePermanent, sigle, noGroupe, codeSession, dateAbandon, et
nombre de jours entre l’abandon et le début de la session
*/
SELECT CODEPERMANENT,SIGLE, NOGROUPE, CODESESSION, DATEABANDON,
       TO_DATE(DATEABANDON,'DD/MM/YYYY') - TO_DATE(DATEINSCRIPTION,'DD/MM/YYYY')
	   AS NOMBRES_JOURS_INSCRITS
FROM   INSCRIPTION
WHERE  DATEABANDON IS NOT NULL;

/***************
 * EXERCICE 2. *
 * REQ # 14.   *
 **************/

-- Afficher la liste des sigle, noGroupe et codeSession des groupes qui n’ont pas d’inscription
SELECT DISTINCT (GC.SIGLE), GC.NOGROUPE, GC.CODESESSION
FROM   GROUPECOURS GC
WHERE  GC.SIGLE NOT IN (SELECT I.SIGLE FROM INSCRIPTION I);

/***************
 * EXERCICE 2. *
 * REQ # 15.   *
 **************/

/*
Afficher la liste en ordre alphabétique croissant des noms et prénoms (sans répétition) de tous les
étudiants qui ont suivi au moins un cours avec le professeur Pascal Blaise
*/
SELECT DISTINCT (E.NOM), E.PRENOM
FROM   ETUDIANT E, INSCRIPTION I
WHERE  E.CODEPERMANENT = I.CODEPERMANENT AND
       I.SIGLE IN(SELECT GC.SIGLE FROM GROUPECOURS GC WHERE GC.CODEPROFESSEUR LIKE 'PASB1') AND
       I.NOGROUPE IN(SELECT GC.NOGROUPE FROM GROUPECOURS GC WHERE GC.CODEPROFESSEUR LIKE 'PASB1')
ORDER BY E.NOM ASC;
